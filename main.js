// Теоретичне питання:

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX означає асинхронний JS і XML. Це метод веб-розробки, який дозволяє зв'язок між
// веб-браузером і сервером без необхідності перезавантажувати всю веб-сторінку. Він покращує
// взаємодію з користувачем; зменшує обсяг даних, що передаються між браузером і сервером. 

// Практичне завдання:

function fetchStarWarsMovies() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://ajax.test-danit.com/api/swapi/films', true);

    xhr.onload = function() {
        if (xhr.status === 200) {
            const movies = JSON.parse(xhr.responseText);
            displayMovies(movies);
        }
    };

    xhr.send();
}

function displayMovies(movies) {
    const moviesContainer = document.getElementById('movies');

    movies.forEach(function(movie) {
        const movieItem = document.createElement('div');
        movieItem.innerHTML = '<h3>Episode' + movie.episodeId + ':' + movie.name + '</h3>' +
        '<p>' + movie.openingCrawl + '</p>' +
        '<div id = "characters_' + movie.episodeId + '></div>';
        moviesContainer.appendChild(movieItem);

        fetchCharacters(movie);
    });
}

function fetchCharacters(movie) {
    const charactersContainer = document.getElementById('characters_' + movie.episodeId);

    movie.characters.forEach(function(characterURL) {
        const characterXHR = new XMLHttpRequest();
        characterXHR.open('GET', characterXHR.open('GET', characterURL, true));

        characterXHR.onload - function() {
            if (characterXHR.status === 200) {
                const character = JSON.parse(characterXHR.responseText);
                const characterItem = document.createElement('p');
                characterItem.innerHTML = character.name;

                charactersContainer.appendChild(characterItem);
            }
        };

        characterXHR.send();
    });
}

fetchStarWarsMovies();